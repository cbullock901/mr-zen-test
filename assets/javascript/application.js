function bh(behaviour) {
 return $('[data-behaviour="' + behaviour + '"]');
}

$(document).ready(function() {

  bh('toggle-mobile-menu').on('click', function(e) {
    e.preventDefault();
    $('header.pageHeader').toggleClass('open');
  });


  var homePage = {

    init: function()
    {
    this.getData();
    this.removeNoJavascriptMessage();
    },

    removeNoJavascriptMessage: function()
    {
      $('.noJavascript').remove();
    },

    getData: function()
    {
      var self = homePage;
      $.ajax({
        url: '/assets/json/data.json',
        dataType: 'json',
        type: 'post',
        success: function(data, status, jqxhr) {
          self.populateSlideShow(data[0].slideshow);
          self.populateCTAS(data[0].holidays);
        },
        error: function(jqxhr, status, errorMsg) {
          if ( console) console.log('Status: ' + status + ' Error: ' + errorMsg);
        }
      });
    },

    populateSlideShow: function(images)
    {
      var self = homePage;
      var source = $("#template-carousel").html();
      var template = Handlebars.compile(source); 
      $(images).each(function(i,el) {
        var context = {
          image: el.image,
          title: el.title,
          desc: el.desc
        }
        var html = template(context);
        $(html).appendTo('#homePageCarousel');
      });
      self.slideShow();
    },

    slideShow: function()
    {
      $("#homePageCarousel").owlCarousel({
        navigation : true,
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true
      });
    },

    populateCTAS: function(holidays)
    {
      var self = homePage;
      var source = $("#template-cta").html();
      var template = Handlebars.compile(source);
      $(holidays).each(function(i,el) {
        var context = {
          title: el.title,
          alias: el.alias,
          country: el.country,
          region: el.region,
          currency: el.currency,
          price: el.price,
          image: el.image
        }
        var html = template(context);
        $(html).appendTo('#homePageCTAs');
      });
    }

  }

  homePage.init();

});